var http = require('http');
const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');
const mongoose = require('mongoose');
const multer = require('multer');
const fs = require('fs');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const userController = require('./controllers/user.controller');
const req = require('express/lib/request');
const fileController = require('./controllers/file.cotroller');
const router = express.Router();
app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(cors());
// app.use(express.static(path.join(__dirname, '..', 'public')));
mongoose.connect('mongodb://localhost:27017/myMachineTask', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => {
    console.log('database connected');
});

const protect = async (req, res, next) => {
  let token;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
      try {
          token = req.headers.authorization.split(' ')[1];
          const decoded = await jwt.verify(token, '123');
          req.user = await userController.findByIdUSer(decoded.id);
          // req.user = await User.findById(decoded.id).select('-password');
          next();
      } catch(err) {
          res.status(401);
          throw new Error('Not Authorized');
      }
  }
  if (!token) {
      res.status(401);
      throw new Error('Not Authorized');
  }
}


app.post('/login', async (req, res) => {
  const { email, password } = req.body;

  const userFindOne = await userController.findUser(email);
  if (!userFindOne) {
      return res.status(400).send({ message:'User not registered' });
  }
  if (await bcrypt.compare(password, userFindOne.password)) {
      userFindOne.token = userController.genToken(userFindOne._id);
      return res.status(200).send({message: 'User login successfully', 
          user: {
              _id: userFindOne.id,
              email: userFindOne.email,
              name: userFindOne.name,
              token: userController.genToken(userFindOne._id),
          }
      });
  }
  return res.status(400).send({message: 'Wrong password'});
});

app.post('/register', async (req, res) => {
  const { name, email, password } = req.body;

  const userFindOne = await userController.findUser(email);
  if (userFindOne) {
      return res.status(404).send({message:'User already exist'});
  }
  const bSalt = await bcrypt.genSalt(10);
  const crypePass = await bcrypt.hash(password, bSalt);
  const user = await userController.register(name,email,crypePass);
  // const user = await new User({
  //     name,
  //     email,
  //     password: crypePass,
  // });
  user.save((err, userSave) => {
      if (err) {
          return res.status(404).send({message: 'User registration Failed'});
      } else {
          return res.status(200).json({
              message:'User register successfully',
              user: {
                  _id: userSave.id,
                  email: userSave.email,
                  name: userSave.name,
                  token: userController.genToken(userSave._id),
              }
          })
      }
  });
});

app.all('/v1/*', protect);

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
      callback(null, './uploads/');
  },
  filename: function (req, file, callback) {
      const fileExtension = file.originalname.split(".")[1];
      const fullFileName = `${file.fieldname}${Math.random(10)}.${fileExtension}`.toLowerCase();
      callback(null, fullFileName);
  }
});

const upload = multer({ storage });

app.get('/v1/listUser', async  function(req, res,next){
  const users = await userController.findAllUser();
  if (!users || !users.length) {
    res.json(users);
      return res.status(204).send({message:'Users not found'});
  }
  return res.status(200).send({message: 'Users retrieved successfully', users});
});



app.post("/v1/uploadFile", upload.single("uploadFile"), async (req, res) => {
  if (!req.file || !req.file.fieldname) {
      return res.status(409).send({ error: 'Something went wrong' });
  }
  fs.readFile(req.file.path, async function (err, data) {
      if (err) {
          return res.status(409).send({ error: 'Error occurred in reading the file' });
      }
      const p_users = req.body.permittedUsers ? req.body.permittedUsers.split(",") : null;
      const permitedUser = p_users ? p_users.map((value)=>{
         return mongoose.Types.ObjectId(value);
      }) :
      null;
      const fileUpload = await fileController.upload(req,permitedUser);
      // const fileUpload = await new FileUpload({
      //     uploadedBy: req.user.id,
      //     fileName: req.file.originalname,
      //     filePath: './' + req.file.path, //'./uploads' + req.file.path
      //     permittedUsers: req.body.permittedUsers ? permitedUser : null,
      // });
      fileUpload.save((err, fileUploadSave) => {
          if (err) {
              return res.status(404).send({message: 'File upload Failed'});
          } else {
              return res.status(200).json({
                  message:'File upload successfully',
                  fileName: req.file.originalname
              })
          }
      });
  });
});

app.get('/v1/listFile', async (req, res) => {
  const files = await fileController.findAllFile();
  if (!files || !files.length) {
      return res.status(204).send({message:'Files not found'});
  }
  return res.status(200).send({message: 'Files retrieved successfully', files});
});


app.get('/v1/fileDetail', async (req, res) => {
  const { userId, fileId } = req.query;
  const { _id } = req.user;
  const file = await fileController.findFile(fileId);
  if (!file) {
      return res.status(404).send({message:'File not found'});
  }


  if (file.uploadedBy.toString() === _id.toString() || file?.permittedUsers?.includes(userId)) {
      fs.readFile(file.filePath, function (err, data) {
          if (err) {
              return res.status(409).send({ error: 'Error occurred in reading the file' });
          }
          return res.status(200).send({message: 'File retrieved successfully', data:{data:data,fileName:file.fileName,mimetype:file.fileMimeType} });
      });
  } else {
      return res.status(200).send({ message: 'You are not authorized to view the file.'})
  }
});

// app.get('/users', async function(req, res, next) {
//   const users = await userController.handleUsers();
//   res.json(users);
// });
//create a server object:
http.createServer(app).listen(5000); //the server object listens on port 8080
