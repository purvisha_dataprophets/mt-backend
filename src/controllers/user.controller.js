const userModel = require('../models/user.model');
const User = require('../models/userModel');
const jwt = require('jsonwebtoken');

module.exports = {
  handleUsers: function() {
    return userModel
      .getAll()
      .then(users => {
        return users.filter(user => {
          return user.id % 5 === 0;
        });
      })
      .catch(err => {
        throw err;
      });
  },

   findAllUser :function () {
    return User.find({})
},

 findUser :function(email) {
  return User.findOne({email: email})
},
 genToken :function(id) {
  return jwt.sign({id}, '123', {
      expiresIn: '30d'
  })
},
register : function(name,email,crypePass) {
return User({
      name,
      email,
      password: crypePass,
})
},
findByIdUSer :function(decodedId){
  return User.findById(decodedId).select('-password');
}

};
