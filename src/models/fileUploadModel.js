const mongoose = require('mongoose');

const fileuploadSchema = new mongoose.Schema({
    uploadedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    filePath: String,
    fileName: String,
    fileMimeType:String,
    permittedUsers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
});

module.exports = new mongoose.model("FileUpload", fileuploadSchema);