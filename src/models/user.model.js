const axios = require('axios');

module.exports = {
  getAll: function() {
    return axios
      .get('http://jsonplaceholder.typicode.com/users')
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  }
};
